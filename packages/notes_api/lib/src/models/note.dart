import 'package:equatable/equatable.dart';
import 'package:json_annotation/json_annotation.dart';
import 'package:meta/meta.dart';
import 'package:notes_api/notes_api.dart';
import 'package:uuid/uuid.dart';

part 'note.g.dart';

/// {@template note}
/// A single note item.
///
/// Contains a [title], [description] and [id], in addition to corresponding [date].
///
/// If an [id] is provided, it cannot be empty. If no [id] is provided, one
/// will be generated.
///
/// [Note]s are immutable and can be copied using [copyWith], in addition to
/// being serialized and deserialized using [toJson] and [fromJson]
/// respectively.
/// {@endtemplate}
@immutable
@JsonSerializable()
class Note extends Equatable {
  /// {@macro note}
  Note({
    String? id,
    required this.title,
    this.description = '',
		DateTime? date,
  })  : assert(
          id == null || id.isNotEmpty,
          'id can not be null and should be empty',
        ),
        id = id ?? const Uuid().v4(),
				date = getOnlyDate(date ?? DateTime.now());

  final String id;

	final DateTime date;
	static DateTime getOnlyDate(DateTime date) {
		return DateTime(date.year, date.month, date.day);
	}

  final String title;
  final String description;


  /// Returns a copy of this note with the given values updated.
  ///
  /// {@macro note}
  Note copyWith({
    String? id,
    String? title,
    String? description,
		DateTime? date,
  }) {
    return Note(
      id: id ?? this.id,
      title: title ?? this.title,
      description: description ?? this.description,
			date: date ?? this.date,
    );
  }

  /// Deserializes the given [JsonMap] into a [Note].
  static Note fromJson(JsonMap json) => _$NoteFromJson(json);

  /// Converts this [Note] into a [JsonMap].
  JsonMap toJson() => _$NoteToJson(this);

  @override
  List<Object> get props => [id, title, description, date];
}
