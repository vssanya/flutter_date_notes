/// A Flutter implementation of the NotesApi that uses local storage.
library local_storage_notes_api;

export 'package:shared_preferences/shared_preferences.dart'
    show SharedPreferences;

export 'src/local_storage_notes_api.dart';
