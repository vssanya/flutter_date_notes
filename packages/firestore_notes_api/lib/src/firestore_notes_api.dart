import 'dart:async';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:meta/meta.dart';
import 'package:rxdart/subjects.dart';
import 'package:notes_api/notes_api.dart';

/// {@template firestore_notes_api}
/// A Flutter implementation of the [NotesApi] that uses local storage.
/// {@endtemplate}
class FirestoreNotesApi extends NotesApi {
  /// {@macro local_storage_notes_api}
  FirestoreNotesApi({
		required FirebaseFirestore firestore
  }) : _firestore = firestore, _notes = firestore.collection('notes') {
    _init();
  }

	final FirebaseFirestore _firestore;
	CollectionReference _notes;

  final _noteStreamController = BehaviorSubject<List<Note>>();

  /// The key used for storing the notes locally.
  ///
  /// This is only exposed for testing and shouldn't be used by consumers of
  /// this library.
  @visibleForTesting
  static const kNotesCollectionKey = '__notes_collection_key__';

  void _init() {
		_notes.get().then((objs) {
			final notes = objs.docs.map((obj) {
        return Note.fromJson(obj.data() as Map<String, dynamic>);
			}).toList();
      _noteStreamController.add(notes);
		});
  }

  @override
  Stream<List<Note>> getNotes() => _noteStreamController.asBroadcastStream();

  @override
  Future<void> saveNote(Note note) async {
    final notes = [..._noteStreamController.value];
    final noteIndex = notes.indexWhere((t) => t.id == note.id);
    if (noteIndex >= 0) {
      notes[noteIndex] = note;
			final query = await _notes.where('id', isEqualTo: note.id).get();
			if (query.docs.first.exists) {
				await _notes.doc(query.docs.first.id).update(note.toJson());
			}
    } else {
      notes.add(note);
			_notes.add(note.toJson());
    }

    _noteStreamController.add(notes);
  }

  @override
  Future<void> deleteNote(String id) async {
    final notes = [..._noteStreamController.value];
    final noteIndex = notes.indexWhere((t) => t.id == id);
    if (noteIndex == -1) {
      throw NoteNotFoundException();
    } else {
      notes.removeAt(noteIndex);
      _noteStreamController.add(notes);

			final query = await _notes.where('id', isEqualTo: id).get();
			if (query.docs.first.exists) {
				await _notes.doc(query.docs.first.id).delete();
			}
    }
  }
}
