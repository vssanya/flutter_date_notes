/// A Flutter implementation of the NotesApi that uses local storage.
library firestore_notes_api;

export 'package:shared_preferences/shared_preferences.dart'
    show SharedPreferences;

export 'src/firestore_notes_api.dart';
