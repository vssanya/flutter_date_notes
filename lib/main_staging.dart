import 'package:flutter_services_binding/flutter_services_binding.dart';
import 'package:flutter_notes/bootstrap.dart';
import 'package:local_storage_notes_api/local_storage_notes_api.dart';

Future<void> main() async {
  FlutterServicesBinding.ensureInitialized();

  final notesApi = LocalStorageNotesApi(
    plugin: await SharedPreferences.getInstance(),
  );

  bootstrap(notesApi: notesApi);
}
