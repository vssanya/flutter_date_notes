part of 'edit_note_bloc.dart';

enum EditNoteStatus { initial, loading, success, failure }

extension EditNoteStatusX on EditNoteStatus {
  bool get isLoadingOrSuccess => [
        EditNoteStatus.loading,
        EditNoteStatus.success,
      ].contains(this);
}

class EditNoteState extends Equatable {
  const EditNoteState({
    this.status = EditNoteStatus.initial,
    this.initialNote,
    this.title = '',
    this.description = '',
		required this.date,
  });

  final EditNoteStatus status;
  final Note? initialNote;
  final String title;
  final String description;
	final DateTime date;

  bool get isNewNote => initialNote == null;

  EditNoteState copyWith({
    EditNoteStatus? status,
    Note? initialNote,
    String? title,
    String? description,
		DateTime? date,
  }) {
    return EditNoteState(
      status: status ?? this.status,
      initialNote: initialNote ?? this.initialNote,
      title: title ?? this.title,
      description: description ?? this.description,
			date: date ?? this.date,
    );
  }

  @override
  List<Object?> get props => [status, initialNote, title, description, date];
}
