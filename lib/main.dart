import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:firestore_notes_api/firestore_notes_api.dart';
import 'package:flutter_services_binding/flutter_services_binding.dart';
import 'package:flutter_notes/bootstrap.dart';
import 'package:flutter_notes/firebase_options.dart';

Future<void> main() async {
	//WidgetsFlutterBinding.ensureInitialized();
  FlutterServicesBinding.ensureInitialized();

	await Firebase.initializeApp(
			options: DefaultFirebaseOptions.currentPlatform,
	);

  final notesApi = FirestoreNotesApi(
		firestore: FirebaseFirestore.instance,
  );

  bootstrap(notesApi: notesApi);
}
