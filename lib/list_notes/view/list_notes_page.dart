import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_notes/edit_note/view/edit_note_page.dart';
import 'package:flutter_notes/l10n/l10n.dart';
import 'package:flutter_notes/list_notes/list_notes.dart';
import 'package:notes_repository/notes_repository.dart';


class ListNotesPage extends StatelessWidget {
  const ListNotesPage({Key? key}) : super(key: key);

  static Route<void> route(DateTime date) {
    return MaterialPageRoute(
      //fullscreenDialog: true,
      builder: (context) => BlocProvider(
					create: (context) => ListNotesBloc(
							notesRepository: context.read<NotesRepository>(),
							date: date,
					)..add(const ListNotesSubscriptionRequested()),
					child: const ListNotesPage(),
			),
    );
  }

  @override
  Widget build(BuildContext context) {
    return const ListNotesView();
  }
}

class ListNotesView extends StatelessWidget {
  const ListNotesView({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final l10n = context.l10n;

    return Scaffold(
      appBar: AppBar(
        title: Text(l10n.notesOverviewAppBarTitle),
      ),
      floatingActionButtonLocation: FloatingActionButtonLocation.centerFloat,
      floatingActionButton: FloatingActionButton(
        key: const Key('homeView_addNote_floatingActionButton'),
        onPressed: () => Navigator.of(context).push(EditNotePage.route(context.read<ListNotesBloc>().date)),
        child: const Icon(Icons.add),
      ),
      body: MultiBlocListener(
        listeners: [
          BlocListener<ListNotesBloc, ListNotesState>(
            listenWhen: (previous, current) =>
                previous.status != current.status,
            listener: (context, state) {
              if (state.status == ListNotesStatus.failure) {
                ScaffoldMessenger.of(context)
                  ..hideCurrentSnackBar()
                  ..showSnackBar(
                    SnackBar(
                      content: Text(l10n.notesOverviewErrorSnackbarText),
                    ),
                  );
              }
            },
          ),
          BlocListener<ListNotesBloc, ListNotesState>(
            listenWhen: (previous, current) =>
                previous.lastDeletedNote != current.lastDeletedNote &&
                current.lastDeletedNote != null,
            listener: (context, state) {
              final deletedNote = state.lastDeletedNote!;
              final messenger = ScaffoldMessenger.of(context);
              messenger
                ..hideCurrentSnackBar()
                ..showSnackBar(
                  SnackBar(
                    content: Text(
                      l10n.notesOverviewNoteDeletedSnackbarText(
                        deletedNote.title,
                      ),
                    ),
                    action: SnackBarAction(
                      label: l10n.notesOverviewUndoDeletionButtonText,
                      onPressed: () {
                        messenger.hideCurrentSnackBar();
                        context
                            .read<ListNotesBloc>()
                            .add(const ListNotesUndoDeletionRequested());
                      },
                    ),
                  ),
                );
            },
          ),
        ],
        child: BlocBuilder<ListNotesBloc, ListNotesState>(
          builder: (context, state) {
            if (state.notes.isEmpty) {
              if (state.status == ListNotesStatus.loading) {
                return const Center(child: CupertinoActivityIndicator());
              } else if (state.status != ListNotesStatus.success) {
                return const SizedBox();
              } else {
                return Center(
                  child: Text(
                    l10n.notesOverviewEmptyText,
                    style: Theme.of(context).textTheme.caption,
                  ),
                );
              }
            }

            return CupertinoScrollbar(
              child: ListView(
                children: [
                  for (final note in state.notes)
                    NoteListTile(
                      note: note,
                      onDismissed: (_) {
                        context
                            .read<ListNotesBloc>()
                            .add(ListNotesNoteDeleted(note));
                      },
                      onTap: () {
                        Navigator.of(context).push(
                          EditNotePage.route(context.read<ListNotesBloc>().date, initialNote: note),
                        );
                      },
                    ),
                ],
              ),
            );
          },
        ),
      ),
    );
  }
}
