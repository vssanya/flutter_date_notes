part of 'list_notes_bloc.dart';

enum ListNotesStatus { initial, loading, success, failure }

class ListNotesState extends Equatable {
  const ListNotesState({
    this.status = ListNotesStatus.initial,
    this.notes = const [],
    this.lastDeletedNote,
  });

  final ListNotesStatus status;
  final List<Note> notes;
  final Note? lastDeletedNote;

  ListNotesState copyWith({
    ListNotesStatus Function()? status,
    List<Note> Function()? notes,
    Note? Function()? lastDeletedNote,
  }) {
    return ListNotesState(
      status: status != null ? status() : this.status,
      notes: notes != null ? notes() : this.notes,
      lastDeletedNote:
          lastDeletedNote != null ? lastDeletedNote() : this.lastDeletedNote,
    );
  }

  @override
  List<Object?> get props => [
        status,
        notes,
        lastDeletedNote,
      ];
}
