part of 'list_notes_bloc.dart';

abstract class ListNotesEvent extends Equatable {
  const ListNotesEvent();

  @override
  List<Object> get props => [];
}

class ListNotesSubscriptionRequested extends ListNotesEvent {
  const ListNotesSubscriptionRequested();
}

class ListNotesNoteDeleted extends ListNotesEvent {
  const ListNotesNoteDeleted(this.note);

  final Note note;

  @override
  List<Object> get props => [note];
}

class ListNotesUndoDeletionRequested extends ListNotesEvent {
  const ListNotesUndoDeletionRequested();
}
