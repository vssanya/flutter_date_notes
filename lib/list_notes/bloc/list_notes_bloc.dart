import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter_notes/list_notes/list_notes.dart';
import 'package:notes_repository/notes_repository.dart';

part 'list_notes_event.dart';
part 'list_notes_state.dart';

class ListNotesBloc extends Bloc<ListNotesEvent, ListNotesState> {
  ListNotesBloc({
    required NotesRepository notesRepository,
		required this.date,
  })  : _notesRepository = notesRepository,
        super(const ListNotesState()) {
    on<ListNotesSubscriptionRequested>(_onSubscriptionRequested);
    on<ListNotesNoteDeleted>(_onNoteDeleted);
    on<ListNotesUndoDeletionRequested>(_onUndoDeletionRequested);
  }

  final NotesRepository _notesRepository;
	final DateTime date;

  Future<void> _onSubscriptionRequested(
    ListNotesSubscriptionRequested event,
    Emitter<ListNotesState> emit,
  ) async {
    emit(state.copyWith(status: () => ListNotesStatus.loading));

    await emit.forEach<List<Note>>(
      _notesRepository.getNotes(),
      onData: (notes) => state.copyWith(
        status: () => ListNotesStatus.success,
        notes: () => notes.where((note) => note.date.difference(date).inDays == 0).toList(),
      ),
      onError: (_, __) => state.copyWith(
        status: () => ListNotesStatus.failure,
      ),
    );
  }

  Future<void> _onNoteDeleted(
    ListNotesNoteDeleted event,
    Emitter<ListNotesState> emit,
  ) async {
    emit(state.copyWith(lastDeletedNote: () => event.note));
    await _notesRepository.deleteNote(event.note.id);
  }

  Future<void> _onUndoDeletionRequested(
    ListNotesUndoDeletionRequested event,
    Emitter<ListNotesState> emit,
  ) async {
    assert(
      state.lastDeletedNote != null,
      'Last deleted note can not be null.',
    );

    final note = state.lastDeletedNote!;
    emit(state.copyWith(lastDeletedNote: () => null));
    await _notesRepository.saveNote(note);
  }
}
