part of 'list_dates_bloc.dart';

enum ListDatesStatus { initial, loading, success, failure }

class ListDatesState extends Equatable {
  const ListDatesState({
    this.status = ListDatesStatus.initial,
		this.dateHasNotes = const <int>{},
  });

  final ListDatesStatus status;
	final Set<int> dateHasNotes; 

  @override
  List<Object> get props => [status, dateHasNotes];

  ListDatesState copyWith({
    ListDatesStatus? status,
    Set<int>? dateHasNotes,
  }) {
    return ListDatesState(
      status: status ?? this.status,
      dateHasNotes:  dateHasNotes ?? this.dateHasNotes,
    );
  }
}
