import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:notes_repository/notes_repository.dart';

part 'list_dates_event.dart';
part 'list_dates_state.dart';

class ListDatesBloc extends Bloc<ListDatesEvent, ListDatesState> {
  ListDatesBloc({
    required NotesRepository notesRepository,
  })  : _notesRepository = notesRepository,
        super(ListDatesState()) {
    on<ListDatesSubscriptionRequested>(_onSubscriptionRequested);
  }

  final NotesRepository _notesRepository;

  Future<void> _onSubscriptionRequested(
    ListDatesSubscriptionRequested event,
    Emitter<ListDatesState> emit,
  ) async {
    emit(state.copyWith(status: ListDatesStatus.loading));

    await emit.forEach<List<Note>>(
      _notesRepository.getNotes(),
      onData: (notes) {
				var now = DateTime.now();
				var dateHasNotes = <int>{};
				notes.forEach((note) {
					dateHasNotes.add(now.difference(note.date).inDays);
				});

				return state.copyWith(
						status: ListDatesStatus.success,
						dateHasNotes: dateHasNotes,
				);
			},
      onError: (_, __) => state.copyWith(status: ListDatesStatus.failure),
    );
  }
}
