part of 'list_dates_bloc.dart';

abstract class ListDatesEvent extends Equatable {
  const ListDatesEvent();

  @override
  List<Object> get props => [];
}

class ListDatesSubscriptionRequested extends ListDatesEvent {
  const ListDatesSubscriptionRequested();
}
