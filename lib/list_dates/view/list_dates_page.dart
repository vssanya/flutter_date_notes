import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_notes/l10n/l10n.dart';
import 'package:flutter_notes/list_dates/list_dates.dart';
import 'package:flutter_notes/list_notes/list_notes.dart';
import 'package:intl/intl.dart';
import 'package:notes_repository/notes_repository.dart';


class ListDatesPage extends StatelessWidget {
  const ListDatesPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (context) => ListDatesBloc(
        notesRepository: context.read<NotesRepository>(),
      )..add(const ListDatesSubscriptionRequested()),
      child: const ListDatesView(),
    );
  }
}

class ListDatesView extends StatelessWidget {
  const ListDatesView({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final l10n = context.l10n;
    final state = context.watch<ListDatesBloc>().state;
    final textTheme = Theme.of(context).textTheme;
		final now = DateTime.now();
		final format = DateFormat('yyyy/MM/dd');
		print(state.status);

    return Scaffold(
      appBar: AppBar(
        title: Text(l10n.statsAppBarTitle),
      ),
      body: state.status == ListDatesStatus.success ? ListView.builder(
					padding: const EdgeInsets.all(8),
					itemBuilder: (context, index) {				
						final date = now.subtract(Duration(days: index));
						return
								GestureDetector(
										onTap: () {
											Navigator.of(context).push(ListNotesPage.route(Note.getOnlyDate(date)));
										},
										child: Container(
															 margin: const EdgeInsets.all(4),
															 height: 50,
															 color: state.dateHasNotes.contains(index) ? Colors.blue[600] : Colors.blue[100],
															 child: Center(child: Text('Date ${format.format(date)}', style: textTheme.headline6)),
													 )
								);
      }) : const Center(child: CircularProgressIndicator()),
    );
  }
}
